package framadatectl

import (
	"encoding/csv"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"time"
)

type FramadateKey struct {
	Date   time.Time
	Moment string
}

type FramadateKeyList []FramadateKey

type FramadateData map[FramadateKey]([]string)

type Framadate struct {
	Url  string
	Data FramadateData
}

func (f FramadateKey) MarshalText() ([]byte, error) {
	date := f.Date.Format("2006-01-02")
	if len(f.Moment) == 0 {
		return []byte(date), nil
	} else {
		return []byte(date + "|" + f.Moment), nil
	}
}

func (f *FramadateKey) UnmarshalText(text []byte) error {
	pos := len(text)
	for i, v := range text {
		if v == '|' {
			pos = i
			break
		}
	}
	date, err := time.Parse("2006-01-02", string(text[:pos]))
	if err != nil {
		return err
	}
	f.Date = date
	if pos != len(text) {
		f.Moment = string(text[pos+1:])
	}
	return nil
}

func (l FramadateKeyList) Len() int {
	return len(l)
}

func (l FramadateKeyList) Less(i, j int) bool {
	if l[i].Date == l[j].Date {
		return l[i].Moment < l[j].Moment
	} else {
		return l[i].Date.Before(l[j].Date)
	}
}

func (l FramadateKeyList) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

type ErrFetch struct {
	Url        string
	StatusCode int
}

type ErrUrl struct {
	Url string
}

func (e *ErrFetch) Error() string {
	return fmt.Sprintf("Getting %s returned the HTTP code %v", e.Url, e.StatusCode)
}

func (e *ErrUrl) Error() string {
	return fmt.Sprintf("URL %s is invalid", e.Url)
}

func getCsvUrl(url string) (string, error) {
	re := regexp.MustCompile(`(https{0,1}://.+)/(\w+)/admin`)
	matches := re.FindStringSubmatch(url)
	if matches != nil {
		base_uri, admin_id := matches[1], matches[2]
		return fmt.Sprintf("%s/exportcsv.php?admin=%s", base_uri, admin_id), nil
	}

	re = regexp.MustCompile(`(https{0,1}://.+)/([^/]+)`)
	matches = re.FindStringSubmatch(url)
	if matches != nil {
		base_uri, id := matches[1], matches[2]
		return fmt.Sprintf("%s/exportcsv.php?poll=%s", base_uri, id), nil
	}

	return "", &ErrUrl{url}
}

func fetch(url string) (io.ReadCloser, error) {
	client := http.Client{}
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Accept-Language", "en-US")
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, &ErrFetch{url, resp.StatusCode}
	}
	return resp.Body, nil
}

func loadCsv(reader io.Reader) (FramadateData, error) {
	r := csv.NewReader(reader)
	records, err := r.ReadAll()
	if err != nil {
		return nil, err
	}
	res := make(map[FramadateKey]([]string))
	slots := make([]FramadateKey, 0, len(records)-1)
	for i, j := range records[0][1 : len(records[0])-1] {
		date, err := time.Parse("2006-01-02", j)
		if err != nil {
			return nil, err
		}
		key := FramadateKey{date, records[1][1 : len(records[0])-1][i]}
		slots = append(slots, key)
		res[key] = make([]string, 0)
	}
	for _, line := range records[2:] {
		name := line[0]
		for i, vote := range line[1 : len(line)-1] {
			if vote == "Yes" {
				res[slots[i]] = append(res[slots[i]], name)
			}
		}
	}
	return res, nil
}

func New(url string) (Framadate, error) {
	res := Framadate{url, nil}
	csvUrl, err := getCsvUrl(url)
	if err != nil {
		return res, err
	}
	reader, err := fetch(csvUrl)
	if err != nil {
		return res, err
	}
	fra, err := loadCsv(reader)
	if err != nil {
		return res, err
	}
	res.Data = fra
	return res, err
}
