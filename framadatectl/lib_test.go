package framadatectl

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"sort"
	"strings"
	"testing"
	"time"
)

const csvStringValid = (`,"2021-09-29","2021-10-06","2021-10-06","2022-01-05",` + "\n" +
	`,"Légumes + oeufs","Légumes","Pommes","",` + "\n" +
	`"albinou","Yes","Yes","No","No",` + "\n" +
	`"Bob","No","No","Yes","No",` + "\n" +
	`"Toto","No","Yes","No","No",` + "\n" +
	`"Titi","Yes","No","No","No",` + "\n")

func getFramadateDataValid() FramadateData {
	return FramadateData{
		FramadateKey{time.Date(2021, time.September, 29, 0, 0, 0, 0, time.UTC), "Légumes + oeufs"}: []string{"albinou", "Titi"},
		FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Légumes"}:           []string{"albinou", "Toto"},
		FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Pommes"}:            []string{"Bob"},
		FramadateKey{time.Date(2022, time.January, 05, 0, 0, 0, 0, time.UTC), ""}:                  []string{},
	}
}

func TestFramadateKeyMarshalText(t *testing.T) {
	framaKey := FramadateKey{time.Date(2021, time.September, 29, 0, 0, 0, 0, time.UTC), "Légumes + oeufs"}
	expectedString := "2021-09-29|Légumes + oeufs"
	res, err := framaKey.MarshalText()
	if err != nil || string(res) != expectedString {
		t.Fatalf("FramadateKey.MarshalText() = %s, %v does not match %s, %v",
			string(res), err, expectedString, nil)
	}
}

func TestFramadateKeyMarshalTextWithoutMoment(t *testing.T) {
	framaKey := FramadateKey{time.Date(2022, time.January, 05, 0, 0, 0, 0, time.UTC), ""}
	expectedString := "2022-01-05"
	res, err := framaKey.MarshalText()
	if err != nil || string(res) != expectedString {
		t.Fatalf("FramadateKey.MarshalText() = %s, %v does not match %s, %v",
			string(res), err, expectedString, nil)
	}
}

func TestFramadateKeyUnmarshalText(t *testing.T) {
	inputString := "2021-09-29|Légumes + oeufs"
	expectedFramaKey := FramadateKey{time.Date(2021, time.September, 29, 0, 0, 0, 0, time.UTC), "Légumes + oeufs"}
	var res FramadateKey
	err := res.UnmarshalText([]byte(inputString))
	if err != nil || res != expectedFramaKey {
		t.Fatalf("FramadateKey.UnmarshalText() = %v, %v does not match %v, %v",
			res, err, expectedFramaKey, nil)
	}
}

func TestFramadateKeyUnmarshalTextWithoutMoment(t *testing.T) {
	inputString := "2022-01-05"
	expectedFramaKey := FramadateKey{time.Date(2022, time.January, 05, 0, 0, 0, 0, time.UTC), ""}
	var res FramadateKey
	err := res.UnmarshalText([]byte(inputString))
	if err != nil || res != expectedFramaKey {
		t.Fatalf("FramadateKey.UnmarshalText() = %v, %v does not match %v, %v",
			res, err, expectedFramaKey, nil)
	}
}

func TestFramadateKeyList(t *testing.T) {
	unsortedList := FramadateKeyList{
		FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Pommes"},
		FramadateKey{time.Date(2022, time.January, 05, 0, 0, 0, 0, time.UTC), ""},
		FramadateKey{time.Date(2021, time.September, 29, 0, 0, 0, 0, time.UTC), "Légumes + oeufs"},
		FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Légumes"},
	}
	sortedList := FramadateKeyList{
		FramadateKey{time.Date(2021, time.September, 29, 0, 0, 0, 0, time.UTC), "Légumes + oeufs"},
		FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Légumes"},
		FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Pommes"},
		FramadateKey{time.Date(2022, time.January, 05, 0, 0, 0, 0, time.UTC), ""},
	}
	sort.Sort(unsortedList)
	if !reflect.DeepEqual(sortedList, unsortedList) {
		t.Fatalf("sort.Sort() = %v does not match %v",
			unsortedList, sortedList)
	}
}

func TestGetCsvUrlValidAdminUrl(t *testing.T) {
	adminId := "ThisIsTheAdminID"
	adminUrl := fmt.Sprintf("https://framadate.org/%s/admin", adminId)
	expectedCsvUrl := fmt.Sprintf("https://framadate.org/exportcsv.php?admin=%s", adminId)
	csvUrl, err := getCsvUrl(adminUrl)
	if csvUrl != expectedCsvUrl || err != nil {
		t.Fatalf("getCsvUrl(\"%s\") = %q, %v does not match %q, %v",
			adminUrl, csvUrl, err, expectedCsvUrl, nil)
	}
}

func TestGetCsvUrlValidPublicUrl(t *testing.T) {
	publicId := "ThisIsThePublicID"
	publicUrl := fmt.Sprintf("https://framadate.org/%s", publicId)
	expectedCsvUrl := fmt.Sprintf("https://framadate.org/exportcsv.php?poll=%s", publicId)
	csvUrl, err := getCsvUrl(publicUrl)
	if csvUrl != expectedCsvUrl || err != nil {
		t.Fatalf("getCsvUrl(\"%s\") = %q, %v does not match %q, %v",
			publicUrl, csvUrl, err, expectedCsvUrl, nil)
	}
}

func TestGetCsvUrlInvalidUrl(t *testing.T) {
	invalidUrl := "https://framadate.org"
	csvUrl, err := getCsvUrl(invalidUrl)
	if csvUrl != "" || err == nil {
		t.Fatalf("getCsvUrl(\"%s\") = %q, %v does not return an error",
			invalidUrl, csvUrl, err)
	}
}

func TestFetchOK(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			t.Fatalf("httpServer: Expect 'GET' but got %s", r.Method)
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(csvStringValid))
	}))
	defer ts.Close()
	res, err := fetch(ts.URL)
	readString := ""
	if err != nil {
		t.Fatalf("fetch(\"%s\") failed with error %v",
			ts.URL, err)
	}
	readBytes, readErr := io.ReadAll(res)
	if readErr != nil {
		t.Fatalf("io.ReadAll() failed with error %v",
			readErr)
	}
	readString = string(readBytes)
	res.Close()
	if readString != csvStringValid {
		t.Fatalf("fetch(\"%s\") read string = %s and does not match %s",
			ts.URL, readString, csvStringValid)
	}
}

func TestFetchErr404(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			t.Fatalf("httpServer: Expect 'GET' but got %s", r.Method)
		}
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte(csvStringValid))
	}))
	defer ts.Close()
	res, err := fetch(ts.URL)
	if res != nil || err == nil {
		t.Fatalf("fetch(\"%s\") = %s, %v does not return an error",
			ts.URL, res, err)
	}
}

func TestLoadCsvOK(t *testing.T) {
	reader := strings.NewReader(csvStringValid)
	fra, err := loadCsv(reader)
	if err != nil || !reflect.DeepEqual(fra, getFramadateDataValid()) {
		t.Fatalf("loadCsv(...) = %v, %v does not match %v, %v",
			fra, err, getFramadateDataValid(), nil)
	}
}
