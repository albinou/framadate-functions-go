module gitlab.com/albinou/framadate-functions-go/framadate2json-service

go 1.17

require (
	github.com/scaleway/scaleway-functions-go v0.0.0-20211103103919-280251df088b
	gitlab.com/albinou/framadate-functions-go/framadatectl v0.0.0-20220102212303-ce3fc877645d
)
