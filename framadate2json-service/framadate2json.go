package main

import (
	"encoding/json"

	"gitlab.com/albinou/framadate-functions-go/framadatectl"
)

func framadate2json(framadate framadatectl.Framadate) (string, error) {
	b, err := json.Marshal(framadate.Data)
	if err != nil {
		return "", err
	}
	return string(b), nil
}
