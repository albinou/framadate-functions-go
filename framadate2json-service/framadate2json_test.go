package main

import (
	"gitlab.com/albinou/framadate-functions-go/framadatectl"
	"testing"
	"time"
)

func getFramadateValid() framadatectl.Framadate {
	return framadatectl.Framadate{
		"http://framadate.example.com",
		framadatectl.FramadateData{
			framadatectl.FramadateKey{time.Date(2021, time.September, 29, 0, 0, 0, 0, time.UTC), "Légumes + oeufs"}: []string{"albinou", "Titi"},
			framadatectl.FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Légumes"}:           []string{"albinou", "Toto"},
			framadatectl.FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Pommes"}:            []string{"Bob"},
			framadatectl.FramadateKey{time.Date(2022, time.January, 05, 0, 0, 0, 0, time.UTC), ""}:                  []string{},
		},
	}
}

const framadateJsonValid = `{"2021-09-29|Légumes + oeufs":["albinou","Titi"],"2021-10-06|Légumes":["albinou","Toto"],"2021-10-06|Pommes":["Bob"],"2022-01-05":[]}`

func TestFramadate2jsonOK(t *testing.T) {
	res, err := framadate2json(getFramadateValid())
	if err != nil || res != framadateJsonValid {
		t.Fatalf("framadate2json(...) = %s, %v does not match %s, %v",
			res, err, framadateJsonValid, nil)
	}
}
