package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/scaleway/scaleway-functions-go/events"
	"github.com/scaleway/scaleway-functions-go/lambda"
	"gitlab.com/albinou/framadate-functions-go/framadatectl"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	framadate_url := request.QueryStringParameters["framadate_url"]
	if framadate_url == "" {
		framadate_url = os.Getenv("FRAMADATE_URL")
	}
	framadate, err := framadatectl.New(framadate_url)
	if err != nil {
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("ERROR(framadatectl.New): %s\n", err),
			StatusCode: http.StatusInternalServerError,
		}, nil
	}
	body, err := framadate2json(framadate)
	if err != nil {
		return events.APIGatewayProxyResponse{
			Body:       fmt.Sprintf("ERROR(framadate2json): %s\n", err),
			StatusCode: http.StatusInternalServerError,
		}, nil
	}
	return events.APIGatewayProxyResponse{
		Body: string(body),
		Headers: map[string]string{
			"content-type": "application/json; charset=utf-8",
		},
		StatusCode: http.StatusOK,
	}, nil
}

func main() {
	lambda.Start(handler)
}
