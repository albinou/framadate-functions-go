# Framadate functions in Go

This project contains my first Go trial.

But who knows? ... Maybe it will become so sexy that everyone will want to use it :-)

## Goal

First goal is to provide a light web interface displaying the results of a Framadate poll.

## How does it work?

It uses the Function As A Service Scaleway infrastructure.
For the purpose of playing with Golang and Scaleway, it is split into 2 services:
- framadate2json
- json2html

The first service fetch the CSV from the Framadate server and makes it available as a JSON file to the second service.
This second service (json2html) formats a very simple HTML page displaying the poll results.

Also, both services are using the framadatectl package.

## How to use it?

Once deployed, the browser must call the json2html service with a "framadate_url" query set to the URL of the Framadate poll.

Example:
```
https://YOUR_FUNCTION_ENDPOINT.functions.fnc.fr-par.scw.cloud?framadate_url=https://framadate.org/YOUR_FRAMADATE_ID
```
