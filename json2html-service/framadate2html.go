package main

import (
	"fmt"
	"sort"

	"gitlab.com/albinou/framadate-functions-go/framadatectl"
)

func Framadate2Html(framadate framadatectl.Framadate) string {
	res := fmt.Sprintf(
		"<html><head><title>Framadate data from %s</title><head><body>",
		framadate.Url)
	res += "<ul>"
	keys := make(framadatectl.FramadateKeyList, 0, len(framadate.Data))
	for k := range framadate.Data {
		keys = append(keys, k)
	}
	sort.Sort(keys)
	for _, k := range keys {
		if k.Moment == "" {
			res += fmt.Sprintf("<li>%s", k.Date.Format("2006-01-02"))
		} else {
			res += fmt.Sprintf("<li>%s - %s", k.Date.Format("2006-01-02"), k.Moment)
		}
		votes := framadate.Data[k]
		if len(votes) > 0 {
			res += "<ul>"
			for _, v := range votes {
				res += fmt.Sprintf("<li>%s</li>", v)
			}
			res += "</ul>"
		}
		res += "</li>"
	}
	res += "</ul></body></html>"
	return res
}
