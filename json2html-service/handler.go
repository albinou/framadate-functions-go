package main

import (
	"net/http"
	"os"

	"github.com/scaleway/scaleway-functions-go/events"
	"github.com/scaleway/scaleway-functions-go/lambda"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	framadateUrl := request.QueryStringParameters["framadate_url"]
	if framadateUrl == "" {
		framadateUrl = os.Getenv("FRAMADATE_URL")
	}
	framadate2jsonServiceUrl := os.Getenv("FRAMADATE2JSON_SERVICE_URL")
	framadate, err := GetFramadate(framadateUrl, framadate2jsonServiceUrl)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}

	return events.APIGatewayProxyResponse{
		Body:       Framadate2Html(framadate),
		StatusCode: http.StatusOK,
	}, nil
}

func main() {
	lambda.Start(handler)
}
