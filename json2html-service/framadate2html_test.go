package main

import (
	"testing"

	"gitlab.com/albinou/framadate-functions-go/framadatectl"
)

const framadateUrl = `https://framadate.org/myFramadateID`
const framadateHtmlStringValid = `<html><head><title>Framadate data from https://framadate.org/myFramadateID</title><head><body><ul><li>2021-09-29 - Légumes + oeufs<ul><li>albinou</li><li>Titi</li></ul></li><li>2021-10-06 - Légumes<ul><li>albinou</li><li>Toto</li></ul></li><li>2021-10-06 - Pommes<ul><li>Bob</li></ul></li><li>2022-01-05</li></ul></body></html>`

func TestFramadate2Html(t *testing.T) {
	framadate := framadatectl.Framadate{framadateUrl, getFramadateDataValid()}
	html := Framadate2Html(framadate)
	if html != framadateHtmlStringValid {
		t.Fatalf("Framadate2Html(%v) = %s does not match %s",
			framadate, html, framadateHtmlStringValid)
	}
}
