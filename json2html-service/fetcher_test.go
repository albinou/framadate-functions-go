package main

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"

	"gitlab.com/albinou/framadate-functions-go/framadatectl"
)

const framadateJsonStringValid = `{"2021-09-29|Légumes + oeufs":["albinou","Titi"],"2021-10-06|Légumes":["albinou","Toto"],"2021-10-06|Pommes":["Bob"],"2022-01-05":[]}`

func getFramadateDataValid() framadatectl.FramadateData {
	return framadatectl.FramadateData{
		framadatectl.FramadateKey{time.Date(2021, time.September, 29, 0, 0, 0, 0, time.UTC), "Légumes + oeufs"}: []string{"albinou", "Titi"},
		framadatectl.FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Légumes"}:           []string{"albinou", "Toto"},
		framadatectl.FramadateKey{time.Date(2021, time.October, 06, 0, 0, 0, 0, time.UTC), "Pommes"}:            []string{"Bob"},
		framadatectl.FramadateKey{time.Date(2022, time.January, 05, 0, 0, 0, 0, time.UTC), ""}:                  []string{},
	}
}

func TestCheckFramadateUrlValid(t *testing.T) {
	framadateUrl := "https://framadate.org/myFramadateID"
	err := checkFramadateUrl(framadateUrl)
	if err != nil {
		t.Fatalf("checkFramadateUrl(\"%s\") = %v does not match %v",
			framadateUrl, err, nil)
	}
}

func TestCheckFramadateUrlEmpty(t *testing.T) {
	framadateUrl := ""
	err := checkFramadateUrl(framadateUrl)
	if err == nil {
		t.Fatalf("checkFramadateUrl(\"%s\") does not return an error",
			framadateUrl)
	}
}

func TestCheckFramadateUrlQuery(t *testing.T) {
	framadateUrl := "https://framadate.org/myFramadateID?variable=attack"
	err := checkFramadateUrl(framadateUrl)
	if err == nil {
		t.Fatalf("checkFramadateUrl(\"%s\") does not return an error",
			framadateUrl)
	}
}

func TestCheckFramadateUrlFragment(t *testing.T) {
	framadateUrl := "https://framadate.org/myFramadateID#anchor"
	err := checkFramadateUrl(framadateUrl)
	if err == nil {
		t.Fatalf("checkFramadateUrl(\"%s\") does not return an error",
			framadateUrl)
	}
}

func TestGetFramadate(t *testing.T) {
	framadateUrl := "https://framadate.org/myFramadateID"
	expectedFramadate := framadatectl.Framadate{framadateUrl, getFramadateDataValid()}
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet {
			t.Fatalf("httpServer: Expect 'GET' but got %s", r.Method)
		}
		if r.URL.Query().Get("framadate_url") != framadateUrl {
			t.Fatalf("httpServer: Expect framadate_url = %s query but got %s",
				framadateUrl, r.URL.Query().Get("framadate_url"))
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(framadateJsonStringValid))
	}))
	defer ts.Close()
	framadate, err := GetFramadate(framadateUrl, ts.URL)
	if err != nil || !reflect.DeepEqual(framadate, expectedFramadate) {
		t.Fatalf("GetFramadate(\"%s\", \"%s\") = %v, %v does not match %v, %v",
			framadateUrl, ts.URL, framadate, err, expectedFramadate, nil)
	}
}
