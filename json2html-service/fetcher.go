package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"gitlab.com/albinou/framadate-functions-go/framadatectl"
)

func checkFramadateUrl(framadateUrl string) error {
	if framadateUrl == "" {
		return fmt.Errorf("ERROR: framadate_url is empty")
	}
	framadateNetUrl, err := url.Parse(framadateUrl)
	if err != nil {
		return err
	}
	if framadateNetUrl.RawQuery != "" || framadateNetUrl.Fragment != "" {
		return fmt.Errorf("ERROR: framadate_url does not look correct")
	}
	return nil
}

func GetFramadate(framadateUrl string, framadate2jsonServiceUrl string) (framadatectl.Framadate, error) {
	res := framadatectl.Framadate{framadateUrl, make(framadatectl.FramadateData)}
	err := checkFramadateUrl(framadateUrl)
	if err != nil {
		return res, err
	}
	framadate2jsonServiceNetUrl, err := url.Parse(framadate2jsonServiceUrl)
	if err != nil {
		return res, err
	}
	q := framadate2jsonServiceNetUrl.Query()
	q.Set("framadate_url", framadateUrl)
	framadate2jsonServiceNetUrl.RawQuery = q.Encode()
	req, err := http.Get(framadate2jsonServiceNetUrl.String())
	if err != nil {
		return res, err
	}
	jsonData, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return res, err
	}
	err = json.Unmarshal(jsonData, &res.Data)
	if err != nil {
		return res, err
	}
	return res, nil
}
